//Specification file for the Player class
#ifndef PLAYER_H
#define PLAYER_H
#include<string>

using namespace std;

class Player;											//Forward declaration for Player class

														//Function Prototypes for Overloaded Stream Operators
ostream &operator << (ostream &, const Player &);
istream &operator >> (istream &, Player &);

class Player
{
private:
	string playerName;				//Player name
	int score;						//Player score

public:
	//Default constructor
	Player()
	{
		playerName = "";
		score = 0;
	}

	//Mutator
	void setName(string name) { playerName = name; }
	void setScore(int scr) { score = scr; }

	//Accessors
	string getName() const { return playerName; }				//getName member function returns name of player
	int getScore() const { return score; }						//getScore member function returns score of player

																//Overloaded member Functions
	Player operator++();										//Overloaded prefix ++ operator
	Player operator ++(int);									//Overloaded postfix ++ operator

	bool operator < (const Player &) const;						//Overloaded relational < operator
	bool operator > (const Player &) const;						//Overloaded relational > operator
	bool operator == (const Player&) const;						//Overloaded relational == operator

																//Friend member functions for << and >> overloaded operators
	friend ostream &operator << (ostream &, const Player &);
	friend istream &operator >> (istream &, Player &);
};
#endif