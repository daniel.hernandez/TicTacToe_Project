//Implementation file for the Player class
#include"GameStats.h"
#include<fstream>
#include<string>

//***********************************************************
// The getGameStats member function sends the player's names*
// and scores to be appended to a file.						*
//***********************************************************
void GameStats::getGameStats(Player &p1, Player &p2)
{
	string name1, name2;
	int score1, score2;

	fstream outputFile("TicTacToe.txt", ios::app);

	name1 = p1.getName();
	name2 = p2.getName();
	score1 = p1.getScore();
	score2 = p2.getScore();

	outputFile << name1 << " vs. " << name2 << " \t\t" << score1 << " - " << score2 << endl;

	outputFile.close();
}