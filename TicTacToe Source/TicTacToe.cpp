//Implementation file for the TicTacToe class
#include<iostream>
#include<string>
#include<stdlib.h>			//Needed for system("cls")
#include "TicTacToe.h"
#include "Player.h"
#include "GameStats.h"

using namespace std;

//***********************************************************
// The constructor initializes the player to "X", the       *
// playerNum to 1, and the TicTacToe board to be played on. *
//***********************************************************
TicTacToe::TicTacToe()
{
	player = 'X';						//Defaults player to X
	playerNum = 1;						//Defaults player number to 1
}

//***********************************************************
// The setPlayerNames member function sets both names to	*
// each player.												*
//***********************************************************
void TicTacToe::setPlayerNames()
{
	cout << "Enter player 1's name: ";

	//Insertion
	cin >> player1;

	cout << "Enter player 2's name: ";
	cin >> player2;
}

//***********************************************************
// The setBoard member function sets the board to its		*
// original state to be played on.                          *
//***********************************************************
void TicTacToe::setBoard()
{
	for (int i = 0; i < 9; i++)
		board[i / 3][i % 3] = '1' + i;
}

//***********************************************************
// The drawBoard member function introduces the game and    *
// displays the board to be played on.						*
//***********************************************************
void TicTacToe::drawBoard()
{
	//Clears displays
	system("cls");

	//Title of game
	cout << "****************************\n"
		<< "*     TIC TAC TOE GAME     *\n"
		<< "****************************\n\n";

	//Shows which player has "X" and "O"
	cout << "Player 1: X	Player 2: O\n"
		<< "\nSCORE" << endl
		<< player1 << ": " << player1.getScore() << "\t\t" << player2 << ": " << player2.getScore() << endl
		<< "___________________________\n\n";

	//Displays board at its current state
	for (int row = 0; row < 3; row++)
	{
		for (int col = 0; col < 3; col++)
		{
			cout << " " << board[row][col] << " ";
		}

		cout << endl << endl;
	}
}

//***********************************************************
// The togglePlayer member function switches between player *
// numbers and game value of "X" or "O".					*
//***********************************************************
void TicTacToe::togglePlayer()
{
	//Switches first player "X" to "O" and 1 to 2
	if (player == 'X')
	{
		player = 'O';
		playerNum = 2;
	}

	//Switches first player "O" to "X" and 2 to 1
	else if (player == 'O')
	{
		player = 'X';
		playerNum = 1;
	}
}

//***********************************************************
// The play member function asks the user what position on  *
// the board they wish to select.							*
//***********************************************************
void TicTacToe::play()
{
	int playerInput;		//Player's choice

							//Player input
	cout << "Player " << playerNum << ", choose a location on the board: ";
	cin >> playerInput;

	//Input validation for number between 1-9
	while (playerInput < 1 || playerInput > 9)
	{
		cout << "PLEASE SELECT A NUMBER DISPLAYED ON THE BOARD: ";
		cin >> playerInput;
	}
	cout << endl;

	//Checks if spot has been taken
	--playerInput;
	while ((board[playerInput / 3][playerInput % 3] == 'X') || (board[playerInput / 3][playerInput % 3] == 'O'))
	{
		cout << "Spot has already been taken, please enter a new location: ";
		cin >> playerInput;
		--playerInput;
	}

	//Places the "X" or "O" in the board array
	board[playerInput / 3][playerInput % 3] = player;

}

//***********************************************************
// The getStatus member function returns an int. Checks for *
// every possible outcome of a winning game.				*
//***********************************************************
int TicTacToe::getStatus() const
{
	// "X's" winning possibilities
	if (board[0][0] == 'X' && board[0][1] == 'X' && board[0][2] == 'X')
		return 1;
	else if (board[1][0] == 'X' && board[1][1] == 'X' && board[1][2] == 'X')
		return 1;
	else if (board[2][0] == 'X' && board[2][1] == 'X' && board[2][2] == 'X')
		return 1;
	else if (board[0][0] == 'X' && board[1][0] == 'X' && board[2][0] == 'X')
		return 1;
	else if (board[0][1] == 'X' && board[1][1] == 'X' && board[2][1] == 'X')
		return 1;
	else if (board[0][2] == 'X' && board[1][2] == 'X' && board[2][2] == 'X')
		return 1;
	else if (board[0][0] == 'X' && board[1][1] == 'X' && board[2][2] == 'X')
		return 1;
	else if (board[0][2] == 'X' && board[1][1] == 'X' && board[2][0] == 'X')
		return 1;

	// "O's" winning possibilities
	if (board[0][0] == 'O' && board[0][1] == 'O' && board[0][2] == 'O')
		return 2;
	else if (board[1][0] == 'O' && board[1][1] == 'O' && board[1][2] == 'O')
		return 2;
	else if (board[2][0] == 'O' && board[2][1] == 'O' && board[2][2] == 'O')
		return 2;
	else if (board[0][0] == 'O' && board[1][0] == 'O' && board[2][0] == 'O')
		return 2;
	else if (board[0][1] == 'O' && board[1][1] == 'O' && board[2][1] == 'O')
		return 2;
	else if (board[0][2] == 'O' && board[1][2] == 'O' && board[2][2] == 'O')
		return 2;
	else if (board[0][0] == 'O' && board[1][1] == 'O' && board[2][2] == 'O')
		return 2;
	else if (board[0][2] == 'O' && board[1][1] == 'O' && board[2][0] == 'O')
		return 2;
}

//***********************************************************
// The determineOutcome member function accepts the int		*
// round variable parameter. Checks the getStatus member	*
// function if "X" or "O" won. Otherwise, it outputs a draw.*
//***********************************************************
void TicTacToe::determineOutcome(int rnd)
{
	int winner;					//Holds getStatus number

								//Calls getStatus for outcome
	winner = getStatus();

	//Determines of "X" or "O" wins
	if (winner == 1)
	{
		cout << player1 << " WINS!";

		//Overloaded prefix ++ operator
		++player1;

		//Shows score for each player & extraction
		cout << "\n\nSCORE" << endl
			<< player1 << ": " << player1.getScore() << "\t\t" << player2.getName() << ": " << player2.getScore() << endl;
	}
	else if (winner == 2)
	{
		cout << player2 << " WINS!";

		//Overloaded postfix ++ operator
		player2++;

		//Shows score for each player & extraction
		cout << "\n\nSCORE" << endl
			<< player1 << ": " << player1.getScore() << "\t\t" << player2.getName() << ": " << player2.getScore() << endl;
	}

	//If rounds equals 9, then game assumes the board is full with no winner
	else if (rnd == 9)
	{
		cout << "IT'S A DRAW!";

		//Shows score for each player & extraction
		cout << "\n\nSCORE" << endl
			<< player1 << ": " << player1.getScore() << "\t\t" << player2.getName() << ": " << player2.getScore() << endl;
	}
}

//***********************************************************
// The grandWinner member function uses overloaded			*
// relational operators to determine winner of all the games*
// played.													*
//***********************************************************
void TicTacToe::grandWinner()
{
	system("cls");

	if (player1 > player2)
	{
		cout << player1 << " is the GRAND WINNER!";
	}

	if (player1 < player2)
	{
		cout << player2 << " is the GRAND WINNER!";
	}

	if (player1 == player2)
	{
		cout << "The ultimate DRAW!";
	}

	//Creates a GameStats object to output to a file
	GameStats game;
	game.getGameStats(player1, player2);
}