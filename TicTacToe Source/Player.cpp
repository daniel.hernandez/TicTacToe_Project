//Implementation file for the Player class
#include"Player.h"
#include<iostream>


//***********************************************************
// Overloaded << operator that displays player name.		*
//***********************************************************
ostream &operator << (ostream &strm, const Player &obj)
{
	strm << obj.playerName;

	return strm;
}

//***********************************************************
// Overloaded >> operator that sets player name.			*
//***********************************************************
istream &operator >> (istream &strm, Player &obj)
{
	strm >> obj.playerName;

	//Cheat code/secret if player chooses the name admin
	if (obj.playerName == "admin")
	{
		obj.setScore(1);
	}

	return strm;
}

//***********************************************************
// Overloaded prefix ++ operator that increments the score.	*
//***********************************************************
Player Player::operator++()
{
	++score;

	return *this;
}

//***********************************************************
// Overloaded postfix ++ operator that increments the score.*
//***********************************************************
Player Player::operator++ (int)
{
	Player temp;

	score++;

	return temp;
}

//***********************************************************
// Overloaded < relational operator that determines who has *
// a higher score.											*
//***********************************************************
bool Player::operator < (const Player &right) const
{
	bool status;

	if (score < right.getScore())
		status = true;
	else
		status = false;

	return status;
}

//***********************************************************
// Overloaded > relational operator that determines who has *
// a higher score.											*
//***********************************************************
bool Player::operator > (const Player &right) const
{
	bool status;

	if (score > right.getScore())
		status = true;
	else
		status = false;

	return status;
}

//***********************************************************
// Overloaded == relational operator that determines if the	*
// scores are equal.
//***********************************************************
bool Player::operator == (const Player &right) const
{
	bool status;

	if (score == right.getScore())
		status = true;
	else
		status = false;

	return status;
}