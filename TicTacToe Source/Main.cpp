/*
Name: DANIEL HERNANDEZ
Date: 04/04/2016
This program is a simple game of TicTacToe. Displays board to be played on and winner of game.
*/
#include<iostream>
#include "TicTacToe.h"

using namespace std;

int main()
{
	TicTacToe game;			//Creates TicTacToe object
	int rounds = 1;			//Count for number of rounds
	char playAgain = 'Y';	//Play again

							//Sets board
	game.setBoard();

	//Call to drawBoard function to create game board
	game.drawBoard();

	//Sets player names
	game.setPlayerNames();

	//Plays game until break is reached
	do
	{
		//Call to play function to get input from user
		game.play();

		//Displays board with updated choices
		game.drawBoard();

		//Checks if a player has won
		game.determineOutcome(rounds);

		//Asks user if they wanna play again
		if (game.getStatus() == 1 || game.getStatus() == 2 || rounds == 9)
		{
			cout << "\n\nWould you like to play again? Y/N: ";
			cin >> playAgain;

			//Validate Y or N answer
			while (toupper(playAgain) != 'Y' && toupper(playAgain) != 'N')
			{
				cout << "Please enter Y or N: ";
				cin >> playAgain;
			}

			//Starts game again
			if (toupper(playAgain) == 'Y')
			{
				rounds = 1;
				game.setBoard();
				game.drawBoard();
				game.play();
				game.drawBoard();
				game.determineOutcome(rounds);
			}
		}

		rounds++;				//Increments rounds  after player choice

								//Switches player when new round starts
		game.togglePlayer();
	} while (toupper(playAgain) == 'Y');

	game.grandWinner();			//Finds grand winner

	return 0;
}