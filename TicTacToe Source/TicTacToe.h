//Specification file for the TicTacToe class
#ifndef TICTACTOE_H
#define TICTACTOE_H
#include "Player.h"

class TicTacToe
{
private:
	char board[3][3];				//TicTacToe Board
	char player;					//Holds player with "X" and player with "O"
	int playerNum;					//Holds player number 1 and 2

									//Create player objects
	Player player1;
	Player player2;

public:
	TicTacToe();					//Default constructor
	void setPlayerNames();			//Sets player's name
	void setBoard();				//Sets board up
	void drawBoard();				//Draws TicTacTow board
	void play();					//Gets user's input to put on board
	void togglePlayer();			//Switches between players
	int getStatus() const;			//Combination of possible winnig outcomes
	void determineOutcome(int);		//Decides outcome of game
	void grandWinner();				//Determines final winner of all games
};
#endif